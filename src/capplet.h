/* 
 * Copyright (C) 2002 Rodney Dawes
 * Copyright 2002 Ximian, Inc.
 *
 * Author:  Rodney Dawes  <dobey@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _VFS_PROXY_CAPPLET_H_
#define _VFS_PROXY_CAPPLET_H_

#include <liboaf/oaf.h>
#include <gconf/gconf-client.h>
#include <config.h>
#include <capplet-widget.h>

typedef struct {
  GtkWidget * window;

  GConfClient * gconf_client;

  GtkWidget * use_proxy;
  GtkWidget * hentry;
  GtkWidget * port;
  GtkWidget * use_pauth;
  GtkWidget * uentry;
  GtkWidget * pentry;

  gboolean proxy;
  gboolean proxy_auth;
  gchar * proxy_host;
  gushort proxy_port;
  gchar * proxy_user;
  gchar * proxy_pass;

  GtkWidget * hlabel;
  GtkWidget * olabel;
  GtkWidget * ulabel;
  GtkWidget * plabel;
} VFSCapplet;

#endif

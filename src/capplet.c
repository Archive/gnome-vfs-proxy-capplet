/* 
 * Copyright (C) 2002 Rodney Dawes
 * Copyright 2002 Ximian, Inc.
 *
 * Author:  Rodney Dawes  <dobey@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "capplet.h"

static gchar * proxy_key = "/system/gnome-vfs/use-http-proxy";
static gchar * pauth_key = "/system/gnome-vfs/use-http-proxy-authorization";
static gchar * phost_key = "/system/gnome-vfs/http-proxy-host";
static gchar * pport_key = "/system/gnome-vfs/http-proxy-port";
static gchar * puser_key = "/system/gnome-vfs/http-proxy-authorization-user";
static gchar * ppass_key = "/system/gnome-vfs/http-proxy-authorization-password";

static ORBit_MessageValidationResult
accept_all_cookies (CORBA_unsigned_long request_id,
		    CORBA_Principal *principal,
		    CORBA_char *operation) {
  /* allow ALL cookies */
  return ORBIT_MESSAGE_ALLOW_ALL;
}

static void vfs_proxy_capplet_load_prefs (VFSCapplet * window) {
  window->proxy = gconf_client_get_bool (window->gconf_client,
					 proxy_key, NULL);
  window->proxy_auth = gconf_client_get_bool (window->gconf_client,
					      pauth_key, NULL);
  window->proxy_host = gconf_client_get_string (window->gconf_client,
						phost_key, NULL);
  window->proxy_port = gconf_client_get_int (window->gconf_client,
					     pport_key, NULL);
  window->proxy_user = gconf_client_get_string (window->gconf_client,
						puser_key, NULL);
  window->proxy_pass = gconf_client_get_string (window->gconf_client,
						ppass_key, NULL);

  GTK_TOGGLE_BUTTON (window->use_proxy)->active = window->proxy;
  GTK_TOGGLE_BUTTON (window->use_pauth)->active = window->proxy_auth;

  if (window->proxy_host != NULL) {
    gtk_entry_set_text (GTK_ENTRY (window->hentry), window->proxy_host);
  }
  if (window->proxy_port > 0) {
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (window->port),
			       window->proxy_port);
  }
  if (window->proxy_user != NULL) {
    gtk_entry_set_text (GTK_ENTRY (window->uentry), window->proxy_user);
  }
  if (window->proxy_pass != NULL) {
    gtk_entry_set_text (GTK_ENTRY (window->pentry), window->proxy_pass);
  }

  if (!window->proxy) {
    gtk_widget_set_sensitive (window->hentry, FALSE);
    gtk_widget_set_sensitive (window->hlabel, FALSE);
    gtk_widget_set_sensitive (window->olabel, FALSE);
    gtk_widget_set_sensitive (window->port, FALSE);
    window->proxy_auth = FALSE;
    GTK_TOGGLE_BUTTON (window->use_pauth)->active = window->proxy_auth;
    gtk_widget_set_sensitive (window->use_pauth, FALSE);
  } else {
    gtk_widget_set_sensitive (window->hentry, TRUE);
    gtk_widget_set_sensitive (window->hlabel, TRUE);
    gtk_widget_set_sensitive (window->olabel, TRUE);
    gtk_widget_set_sensitive (window->port, TRUE);
    gtk_widget_set_sensitive (window->use_pauth, TRUE);
  }
  if (!window->proxy_auth) {
    gtk_widget_set_sensitive (window->uentry, FALSE);
    gtk_widget_set_sensitive (window->pentry, FALSE);
    gtk_widget_set_sensitive (window->ulabel, FALSE);
    gtk_widget_set_sensitive (window->plabel, FALSE);
  } else {
    gtk_widget_set_sensitive (window->uentry, TRUE);
    gtk_widget_set_sensitive (window->pentry, TRUE);
    gtk_widget_set_sensitive (window->ulabel, TRUE);
    gtk_widget_set_sensitive (window->plabel, TRUE);
  }  

  g_free (window->proxy_user);
  g_free (window->proxy_pass);
  window->proxy_user = NULL;
  window->proxy_pass = NULL;
}

static void vfs_proxy_capplet_save_prefs (CappletWidget * capplet,
					  VFSCapplet * window) {
  window->proxy = GTK_TOGGLE_BUTTON (window->use_proxy)->active;
  window->proxy_auth = GTK_TOGGLE_BUTTON (window->use_pauth)->active;
  window->proxy_host = gtk_entry_get_text (GTK_ENTRY (window->hentry));
  window->proxy_port = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (window->port));
  window->proxy_user = g_strdup (gtk_entry_get_text (GTK_ENTRY (window->uentry)));
  window->proxy_pass = g_strdup (gtk_entry_get_text (GTK_ENTRY (window->pentry)));

  gconf_client_set_bool (window->gconf_client, proxy_key,
			 window->proxy, NULL);
  gconf_client_set_bool (window->gconf_client, pauth_key,
			 window->proxy_auth, NULL);
  if (window->proxy) {
    gconf_client_set_string (window->gconf_client, phost_key,
			     window->proxy_host, NULL);
    gconf_client_set_int (window->gconf_client, pport_key,
			  window->proxy_port, NULL);
  } else {
    gconf_client_unset (window->gconf_client, phost_key, NULL);
    gconf_client_unset (window->gconf_client, pport_key, NULL);
  }
  if (window->proxy_auth) {
    gconf_client_set_string (window->gconf_client, puser_key,
			     window->proxy_user, NULL);
    gconf_client_set_string (window->gconf_client, ppass_key,
			     window->proxy_pass, NULL);
  } else {
    gconf_client_unset (window->gconf_client, puser_key, NULL);
    gconf_client_unset (window->gconf_client, ppass_key, NULL);
  }

  gconf_client_suggest_sync (window->gconf_client, NULL);

  g_free (window->proxy_user);
  g_free (window->proxy_pass);
  window->proxy_user = NULL;
  window->proxy_pass = NULL;
}

static void proxy_capplet_prop_changed (GtkWidget * widget,
					VFSCapplet * window) {
  capplet_widget_state_changed (CAPPLET_WIDGET (window->window), FALSE);
  window->proxy = GTK_TOGGLE_BUTTON (window->use_proxy)->active;
  window->proxy_auth = GTK_TOGGLE_BUTTON (window->use_pauth)->active;
  if (!window->proxy) {
    gtk_widget_set_sensitive (window->hentry, FALSE);
    gtk_widget_set_sensitive (window->hlabel, FALSE);
    gtk_widget_set_sensitive (window->olabel, FALSE);
    gtk_widget_set_sensitive (window->port, FALSE);
    window->proxy_auth = FALSE;
    GTK_TOGGLE_BUTTON (window->use_pauth)->active = window->proxy_auth;
    gtk_widget_set_sensitive (window->use_pauth, FALSE);
  } else {
    gtk_widget_set_sensitive (window->hentry, TRUE);
    gtk_widget_set_sensitive (window->hlabel, TRUE);
    gtk_widget_set_sensitive (window->olabel, TRUE);
    gtk_widget_set_sensitive (window->port, TRUE);
    gtk_widget_set_sensitive (window->use_pauth, TRUE);
  }
  if (!window->proxy_auth) {
    gtk_widget_set_sensitive (window->uentry, FALSE);
    gtk_widget_set_sensitive (window->pentry, FALSE);
    gtk_widget_set_sensitive (window->ulabel, FALSE);
    gtk_widget_set_sensitive (window->plabel, FALSE);
  } else {
    gtk_widget_set_sensitive (window->uentry, TRUE);
    gtk_widget_set_sensitive (window->pentry, TRUE);
    gtk_widget_set_sensitive (window->ulabel, TRUE);
    gtk_widget_set_sensitive (window->plabel, TRUE);
  }  
}

static void proxy_port_char_changed (GtkWidget * widget, gpointer data) {
  gchar * foo;

  foo = g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));

  if (foo != NULL && *foo) {
    if (foo[strlen (foo) - 1] < 48 || foo[strlen (foo) - 1] > 57) {
      gtk_entry_set_text (GTK_ENTRY (widget),
			  g_strstrip (g_strndup (foo, strlen (foo) - 1)));
    }
    g_free (foo);
  }

}

static void vfs_proxy_capplet_create_window (void) {
  VFSCapplet * window;
  GtkWidget * hbox, * vbox;
  GtkWidget * lbox, * rbox;
  GtkObject * spinadj;
  GError * gconf_error = NULL;

  window = g_new0 (VFSCapplet, 1);

  window->gconf_client = gconf_client_get_default ();
  window->proxy_port = 0;

  if (!window->gconf_client)
    g_error ("cannot create gconf_client\n");
  gconf_client_add_dir (window->gconf_client,
			"/system/gnome-vfs",
			GCONF_CLIENT_PRELOAD_ONELEVEL, &gconf_error);

  window->window = capplet_widget_new ();
  gtk_signal_connect (GTK_OBJECT (window->window), "ok",
		      GTK_SIGNAL_FUNC (vfs_proxy_capplet_save_prefs), window);

  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (window->window), vbox);

  window->use_proxy = gtk_check_button_new_with_label (_("Use HTTP Proxy"));
  gtk_container_add (GTK_CONTAINER (vbox), window->use_proxy);
  gtk_signal_connect (GTK_OBJECT (window->use_proxy), "clicked",
		      GTK_SIGNAL_FUNC (proxy_capplet_prop_changed), window);
  gtk_widget_show (window->use_proxy);

  hbox = gtk_hbox_new (TRUE, 4);
  gtk_container_add (GTK_CONTAINER (vbox), hbox);

  lbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (hbox), lbox);
  rbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (hbox), rbox);

  window->hlabel = gtk_label_new (_("Proxy Hostname:"));
  gtk_container_add (GTK_CONTAINER (lbox), window->hlabel);
  gtk_misc_set_alignment (GTK_MISC (window->hlabel), 1.0, 0.5);
  gtk_widget_show (window->hlabel);

  window->hentry = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER (rbox), window->hentry);
  gtk_signal_connect (GTK_OBJECT (window->hentry), "changed",
		      GTK_SIGNAL_FUNC (proxy_capplet_prop_changed), window);
  gtk_widget_show (window->hentry);

  window->olabel = gtk_label_new (_("Proxy Port:"));
  gtk_container_add (GTK_CONTAINER (lbox), window->olabel);
  gtk_misc_set_alignment (GTK_MISC (window->olabel), 1.0, 0.5);
  gtk_widget_show (window->olabel);

  spinadj = gtk_adjustment_new (8000, 0, 65535, 5, 5, 3);
  window->port = gtk_spin_button_new (GTK_ADJUSTMENT (spinadj), 5, 0);
  gtk_container_add (GTK_CONTAINER (rbox), window->port);
  gtk_signal_connect (GTK_OBJECT (window->port), "changed",
		      GTK_SIGNAL_FUNC (proxy_capplet_prop_changed), window);
  gtk_widget_show (window->port);
  gtk_widget_show (lbox);
  gtk_widget_show (rbox);
  gtk_widget_show (hbox);

  window->use_pauth = gtk_check_button_new_with_label (_("Use HTTP Proxy Authentication"));
  gtk_container_add (GTK_CONTAINER (vbox), window->use_pauth);
  gtk_signal_connect (GTK_OBJECT (window->use_pauth), "clicked",
		      GTK_SIGNAL_FUNC (proxy_capplet_prop_changed), window);
  gtk_widget_show (window->use_pauth);

  hbox = gtk_hbox_new (TRUE, 4);
  gtk_container_add (GTK_CONTAINER (vbox), hbox);
  lbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (hbox), lbox);
  rbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (hbox), rbox);

  window->ulabel = gtk_label_new (_("Username:"));
  gtk_container_add (GTK_CONTAINER (lbox), window->ulabel);
  gtk_misc_set_alignment (GTK_MISC (window->ulabel), 1.0, 0.5);
  gtk_widget_show (window->ulabel);

  window->uentry = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER (rbox), window->uentry);
  gtk_signal_connect (GTK_OBJECT (window->uentry), "changed",
		      GTK_SIGNAL_FUNC (proxy_capplet_prop_changed), window);
  gtk_widget_show (window->uentry);
  gtk_widget_show (hbox);

  window->plabel = gtk_label_new (_("Password:"));
  gtk_container_add (GTK_CONTAINER (lbox), window->plabel);
  gtk_misc_set_alignment (GTK_MISC (window->plabel), 1.0, 0.5);
  gtk_widget_show (window->plabel);

  window->pentry = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER (rbox), window->pentry);
  gtk_signal_connect (GTK_OBJECT (window->pentry), "changed",
		      GTK_SIGNAL_FUNC (proxy_capplet_prop_changed), window);
  gtk_entry_set_visibility (GTK_ENTRY (window->pentry), FALSE);
  gtk_widget_show (window->pentry);
  gtk_widget_show (lbox);
  gtk_widget_show (rbox);
  gtk_widget_show (hbox);

  vfs_proxy_capplet_load_prefs (window);

  gtk_widget_show (vbox);
  gtk_widget_show (window->window);
}

gint main (gint argc, gchar *argv[]) {
  poptContext ctx;
  GError * gconf_error = NULL;

  gnome_capplet_init ("gnome-vfs-proxy-capplet", VERSION, argc, argv,
		      NULL, 0, &ctx);

  /* Shut ORBit up */
  ORBit_set_request_validation_handler (accept_all_cookies);

  if (!gconf_init (argc, argv, &gconf_error)) {
    g_assert (gconf_error != NULL);
    g_error ("GConf init failed:\n  %s", gconf_error->message);
    return 1;
  }

  vfs_proxy_capplet_create_window ();

  gtk_main ();

  return 0;
}
